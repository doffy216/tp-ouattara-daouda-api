<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Personne>
 */
class PersonneFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "langue" => fake()->word(),
            "genre" => fake()->randomElement(['Masculin','Feminin']),
            "religion" => fake()->word(),
            "pays" => fake()->word(),
            "indicatif" => fake()->randomElement(['ET','FR','CI','US','MO','CA']),
            "internet" => fake()->boolean(),
        ];
    }
}
