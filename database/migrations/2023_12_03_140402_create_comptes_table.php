<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comptes', function (Blueprint $table) {
            $table->id();
            $table->foreignId("abonne_id")->constrained();
            $table->string("libelle");
            $table->string("description");
            $table->string("agence");
            $table->string("banque");
            $table->string("numero");
            $table->string("rib");
            $table->string("montant");
            $table->string("domiciliation");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comptes');
    }
};
